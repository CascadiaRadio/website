[![Netlify Status](https://api.netlify.com/api/v1/badges/24fbacb4-5c1a-4e9f-bf6c-ff619948ac47/deploy-status)](https://app.netlify.com/sites/cascadiaradio/deploys)

---

The [CascadiaRadio.org](https://www.cascadiaradio.org/) website is powered by the [Hugo] Static Site Generator.

The website build/hosting aspects are handled by [Netlify](https://app.netlify.com/sites/cascadiaradio/deploys). This website does not use the GitLab Pages/CI system.

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Netlify](#netlify)
- [Development and Staging Process](#development-and-staging-process)
- [Building locally](#building-locally)
    - [Preview your site](#preview-your-site)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Netlify

This project's static Pages are built and hosted by [Netlify](https://app.netlify.com/sites/cascadiaradio/deploys), following the steps
defined in [`.netlify.toml`](.netlify.toml).

Netlify hosts the following domains for this project:

- https://www.cascadiaradio.org/ (Primary Production Domain)
- https://cascadiaradio.org/ (Alias/Redirect Production Domain)
- https://staging.cascadiaradio.org/ (Development/Staging Domain)
- https://cascadiaradio.netlify.app/ (Default Domain, not used)

TLS Certificates for all domains are automatically managed by Netlify from [Let's Encrypt](https://letsencrypt.org/)

## Development and Staging Process

* The [staging][] branch is built and automatically deployed to https://staging.cascadiaradio.org/
* The [master][] branch is built and automatically deployed to https://www.cascadiaradio.org/

Any changes to the site content should first be commited to the [staging][] branch, and the resulting [staging deployment][] reviewed for errors.  
Once the changes are deemed acceptable, a merge request should be created for those changes from the [staging][] branch to the [master][] branch.  
Once approved, Netlify will perform update the [production deployment][] automatically.

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] Hugo (The **extended** version is required, and version 0.72 or higher is recommended)
1. Preview your project: `hugo server`
1. Add content
1. Generate the website: `hugo` (optional)

Read more at Hugo's [documentation][].

### Preview your site

If you clone or download this project to your local computer and run `hugo server`,
your site can be accessed under `localhost:1313/hugo/`.

The theme used is adapted from https://themes.gohugo.io/theme/airspace-hugo/.

[ci]: https://about.gitlab.com/gitlab-ci/
[hugo]: https://gohugo.io
[install]: https://gohugo.io/overview/installing/
[documentation]: https://gohugo.io/overview/introduction/
[userpages]: http://doc.gitlab.com/ee/pages/README.html#user-or-group-pages
[projpages]: http://doc.gitlab.com/ee/pages/README.html#project-pages
[post]: https://about.gitlab.com/2016/04/07/gitlab-pages-setup/#custom-domains
[production deployment]: https://www.cascadiaradio.org/
[staging deployment]: https://staging.cascadiaradio.org/
[master]: https://gitlab.com/CascadiaRadio/website/-/tree/master
[staging]: https://gitlab.com/CascadiaRadio/website/-/tree/staging
